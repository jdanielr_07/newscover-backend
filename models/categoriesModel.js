const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  id: { type: Int32Array },
  name: { type: String }
});

const CategoryModel = mongoose.model('Category', CategorySchema);

module.exports = {
  model: CategoryModel,
  schema: CategorySchema
}