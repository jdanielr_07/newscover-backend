const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RolSchema = new Schema({
  id: { type: Int32Array },
  name: { type: String }
});

const RolModel = mongoose.model('rols', RolSchema);

module.exports = {
  model: RolModel,
  schema: RolSchema
}